#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemple case
#  $ prog case

ERR_ARG=1
ERR_MES=2

if [ $# -ne 1 ]
then
 echo "Error: numero d'arguments incorrecte"
 echo "usage: $0 dia de la semana"
 exit $ERR_ARG
fi


if [ $1 -lt 1 -o $1 -gt 12 ]
then
 echo "ERROR: Variable incorrecte"
 echo "usage: $0 num [1-12]"
 exit $ERR_MES
fi

case $1 in
  "4"|"6"|"9"|"11")
   echo "el mes $1 té 30 dies"
   ;;
  "2")
   echo "el mes $1 té 28 dies"
   ;;
  *)
   echo "el mes $1 té 31 dies"
   ;;
esac
exit 0
