#! /bin/bash
# @edt ASIX-M01
# Març 2023
#
# Exemples bucle while:
# ---------------------------------------------------------

#7) Numera y muestra en mayusculas la entrada estandar


comptador=1

while read -r line
do
 echo "$comptador:$line" | tr "a-z" "A-Z"
 ((comptador++))
done
exit 0


#-----------------------------------------------------------

#6) Mostrar stdin linia a linia fins token FI

read -r line
while [ "$line" != "FI" ]
do
 echo $line
 read -r line
done
exit 0
#----------------------------------------------------------

#5) Mostrar numerada la entrada estandar

comptador=1

while read -r line
do
 echo "$comptador:$line"
 ((comptador++))
done
exit 0


#----------------------------------------------------------

#4) Procesar entrada estandar linea a linea

while read -r line
do
 echo $line
done
exit 0

#---------------------------------------------------------

#3) iterar per la llista d'arguments

while [ -n "$1" ]
do
 echo "$1 $# $*"
 shift
done
exit 0

#----------------------------------------------------------

#2) Comptador decreixent n(arg)...0
MIN=0
num=$1

while [ $num -ge $MIN ]
do
 echo "$num"
 ((num--))
done
exit 0

#--------------------------------------------------------------

#1) Mostrar del 1 al 10
MAX=10
num=1
while [ $num -le $MAX ]
do
 echo "$num"
 ((num++))
done
exit 0
