#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemple bucles
#  $ prog bucle

#8)Llistar tots els logins numerats
llista_logins=$(cut -d: -f1 /etc/passwd | sort)
num=1

for login in $llista_logins
do
 echo "$num: $login"
 ((num++))
done
exit 0

#7)Numera els fitxers que hi ha al directori actiu
llista=$(ls)
num=1

for nom in $llista
do
 echo "$num: $nom"
 ((num++))
done
exit 0

#6) numerar els arguments 
llista=$*
num=1

for arguments in $llista
do
 echo "$num: $arguments"
 num=$((num+1))
done
exit 0


#5) Iterar i mostrar la llista d'arguments 
llista="$@"
for arguments in $llista
do
 echo "$arguments"
done
exit 0

#4) Iterar i mostrar la llista d'arguments 
llista=$*
for arguments in $llista
do
 echo "$arguments"
done
exit 0


#3) Iterar pel valor d'una variable
llistat=$(ls)
for nom  in $llistat
do
 echo "$nom"
done
exit 0

#2) Iterar per un element 
for nom in "pere marta anna pau"
do
 echo $nom
done
exit 0

#1) iterar per un conjunt d'elements

for nom in "pere" "marta" "anna" "pau"
do
 echo $nom
done
exit 0
