#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemple if: Indicar si es major d'edat o menor d'edat
#  $ prog edat
# ---------------------------------------------------------

# 1) Validar que existeix un argument

if [ $# -ne 1 ]
then
 echo "Error: numero args incorrecte"
 echo "usage: $0 edat"
 exit 1
fi

# 2) xixa

edat=$1
if [ $edat -lt 18 ]
then
 echo "edat $edat és menor d'edat"
elif [ $edat -lt 65 ]
then
 echo "edat $edat forma part de la població activa"
else
 echo "edat $edat és edat de jubilació"
fi
exit 0

