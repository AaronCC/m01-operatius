#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemple acumulador
#  $ prog opcions i arguments
# --------------------------------------------

opcions=""
arguments=""

for arg in $*
do
 case $arg in
  "-a"|"-b"|"-c"|"-d")
   opcions="$opcions $arg";;
  *)
   arguments="$arguments $arg";;
 esac
done
echo "opcions: $opcions"
echo "arguments: $arguments"
exit 0
