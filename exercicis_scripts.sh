#! /bin/bash
# @edt ASIX-M01
# Març 2023
#
# Exercicis d'escripts:

# Exercici 6

laboral=0
festiu=0

for arg in $*
do
case $arg in
 "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
   echo "$arg és laboral"
   ((laboral++))
  ;;
 
 "dissabte"|"diumenge")
   echo "$arg és festiu"
   ((festiu++))
   ;;
  *)
   echo "$0 $arg no es un dia campeón"
   ;;
esac
done
 echo "Hi ha $laboral dies laborals"
 echo "Hi ha $festiu dies festius"
exit 0

# Exercici 5

while read -r line
do
 echo "$line" | cut -c1-50
done
exit 0

# Exercici 4
for mes in $1
do
 case $mes in
 "2")
  dies=28
  ;;
 "4"|"6"|"9"|"11")
  dies=30
  ;;
 *)
  dies=31
  ;;
 esac
 echo "El mes $mes té $dies dies"
done
exit 0

# Exercici 3

MAX=$@
num=0
while [ $num -le $MAX ]
do
 echo "$num"
 ((num++))
done
exit 0

# Exercici 2

num=1

for arg in $@
do
 echo "$num:$arg"
 ((num++))
done
exit 0

# Exercici 1

comptador=1

while read -r line
do
 echo "$comptador:$line"
 ((comptador++))
done
exit 0
