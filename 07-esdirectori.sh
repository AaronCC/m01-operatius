#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemple dir: Validar si l'argument es un directori
#  $ prog directori

ERR_ARG=1
ERR_DIR=2

#1) Validació d'arguments
if [ $# -ne 1 ]
then
 echo "Error: numero d'arguments incorrecte"
 echo "usage: $0 directori"
 exit $ERR_ARG
fi

if ! [ -d $1 ]
then
 echo "Error: Argument invalid, introdueix un directori"
 echo "usage: $0 directori"
 exit $ERR_DIR
fi
#2) xixa
dir=$1
ls $dir
exit 0

