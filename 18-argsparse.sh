#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemple acumulador
#  $ prog opcions i arguments
# --------------------------------------------

opcions=""
arguments=""
myfile=""
num=""
while [ -n "$1" ]
do
 case $1 in
  -[abcd]) opcions="$opcions $1";;
  -f) myfile="$myfile $2"
      shift;;
  -n) num="$num $2"
      shift;;
  *) arguments="$arguments $1";;
 esac
 shift
done
echo "opcions:$opcions"
echo "arguments:$arguments"
echo "files:$myfile"
echo "num:$num"
exit 0
