#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemple case
#  $ prog case

ERR_ARG=1

if [ $# -ne 1 ]
then
 echo "Error: numero d'arguments incorrecte"
 echo "usage: $0 dia de la semana"
 exit $ERR_ARG
fi


case $1 in
 "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
   echo "$1 és laboral"
  ;;
 
 "dissabte"|"diumenge")
   echo "$1 és festiu"
   ;;
  *)
   echo "$1 no es un dia campeón"
   ;;
esac
exit 0


case $1 in
 [aeiou])
   echo "és vocal"
   ;;
  [bcdfghjklmnpqrstvwxyz])
   echo "és consonant"
   ;;
  *)
   echo "és otra cosa"
   ;;
esac
exit 0

case $1 in
 "pere"|"pau"|"joan")
   echo "és un nen"
   ;;
 "marta"|"anna"|"julia")
   echo "és una nena"
   ;;
 *)
   echo "no binari"
   ;;
esac
exit 0
