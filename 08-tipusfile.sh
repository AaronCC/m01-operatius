#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exem
# Normes:
#   shebang (#!) indica qui ha d'interpretar el fitxer
#   caçalera: descripció, data, autor
# --------------------------------------

ERR_ARG=1
ERR_NOEXIST=2

#1) Validació d'arguments
if [ $# -ne 1 ]
then
 echo "Error: numero d'arguments incorrecte"
 echo "usage: $0 fitxer"
 exit $ERR_ARG
fi

if ! [ -a $1 ]
then
 echo "Error: Argument invalid, introdueix un fitxer"
 echo "usage: $0 fitxer"
 exit $ERR_DIR


