#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Exemple validar nota:
#  $ prog nota
# ---------------------------------------------------------

# 1) Validar que existeix un argument

if [ $# -ne 1 ]
then
 echo "Error: numero args incorrecte"
 echo "usage: $0 nota"
 exit 1
fi

if [ $1 -gt 10 -o $1 -lt 0 ]
then
 echo "ERROR: valor incorrecte"
 echo "Usage: $0 nota d'un valor entre 0 i 10"
 exit 2
fi

#2) xixa

if [ $1 -lt 5 ]
then
 echo "La nota $1 és suspes"
elif [ $1 -lt 7 ]
then
 echo "La nota $1 és aprobat"
elif [ $1 -lt 9 ]
then
 echo "La nota $1 és notable"
else
 echo "La nota $1 és excel.lent"
fi

